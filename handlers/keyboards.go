package handlers

import (
	tb "gitlab.com/bonch.dev/go-lib/telebot"
)

const addProducts = "🥑 Добавить продукты 🥑"

var AddProducts = tb.ReplyButton{
	Text: addProducts,
}

const cook = "👩‍🍳 Готовить 👨‍🍳"

var Cook = tb.ReplyButton{
	Text: cook,
}

const favourite = "😋 Избранные рецепты 😋"

var Favourite = tb.ReplyButton{
	Text: favourite,
}

const fridge = "🔒 Продукты в холодильнике 🔒"

var Fridge = tb.ReplyButton{
	Text: fridge,
}

const delivery = "🛒 Доставка 🛒"

var Delivery = tb.ReplyButton{
	Text: delivery,
}

const vip = "👑 VIP 👑"

var VIP = tb.ReplyButton{
	Text: vip,
}

const balance = "💰 Баланс 💰"

var Balance = tb.ReplyButton{
	Text: balance,
}

const beChief = "👨‍🍳 Стать шеф-поваром! 👩‍🍳"

var BeChief = tb.ReplyButton{
	Text: beChief,
}

const back = "Назад"

var Back = tb.ReplyButton{
	Text: back,
}

var InlineBack = tb.InlineButton{
	Unique: "back",
	Text:   back,
}

const wannaCook = "Готовлю!"
const wannaCookPaid = "Купить за %d 💠 и готовить!"

var InlineCook = tb.InlineButton{
	Unique: "wannaCook",
	Text:   wannaCook,
}

var mainKeyboard = [][]tb.ReplyButton{
	{
		AddProducts,
		Cook,
	}, {
		Fridge,
		Favourite,
	}, {
		Delivery,
		VIP,
	}, {
		Balance,
	},
	{
		BeChief,
	},
}
